package ru.bav.animals;

public class Cat extends Animal {


    public Cat() {
        this("Кошка", "Meow");
    }


    protected Cat(String name, String voice) {
        super(name, voice);
    }

}