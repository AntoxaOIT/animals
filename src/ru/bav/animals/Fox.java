package ru.bav.animals;

public class Fox extends Animal {


    public Fox() {
        this("Лиса", "Фррр");
    }


    protected Fox(String name, String voice) {
        super(name, voice);
    }

}