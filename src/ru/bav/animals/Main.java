package ru.bav.animals;

/**
 * @author Barinov group 15OIT18.
 */
public class Main {
    public static void main(String... args) {
        Dog dog = new Dog();
        Dog dog2 = new Dog("Тузик", "тяф-тяф");


        dog.printDisplay();
        dog2.printDisplay();


        Cat cat = new Cat();
        Cat cat2 = new Cat("Пес", "мя-мя");


        cat.printDisplay();
        cat2.printDisplay();

        Fox fox = new Fox();
        Fox fox2 = new Fox("Владислав", "Ррр");

        fox.printDisplay();
        fox2.printDisplay();
    }
}